import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';

import App from './App';
import GlobalView from './Views/GlobalView'
import './index.css';

/* @todo Create route base by Lessons ID. */
ReactDOM.render(
  <BrowserRouter>
    <div>
      <Route exact path="/" component={App} />
      <Route exact path="/lesson/:lessonId" component={GlobalView}/>
    </div>
  </BrowserRouter>,
  document.getElementById('root')
);
