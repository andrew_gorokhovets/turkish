import getRandomElementsFromArray from 'lib/getRandomElementsFromArray';

export default (arr, n) => {
  return getRandomElementsFromArray(arr, n);
}