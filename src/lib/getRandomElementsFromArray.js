/**
 * Function Get random element From Array
 * @todo refactoring with lodash solution.
 *
 * @param {Array} arr
 * @param {String} n
 * @returns {Array|String|Object}
 * @constructor {inherit}
 */
export default (arr, n) => {
  let result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len) {
    throw new RangeError("getRandom: more elements taken than available");
  }
  while (n--) {
    let x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len;
  }
  return result;
}