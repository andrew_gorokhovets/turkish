import React from 'react';
import {get} from 'lodash';
import PropTypes from 'prop-types';

import HeaderComponent from 'components/HeaderComponent';
import NativeText from 'components/NativeText';
import AnswersVariants from 'components/AnswersVariants';
import TranslationTextArea from 'components/TranslationTextArea';
import LessonsData from 'lessons/LessonsData';
import vocabulary from 'data/vocabulary';


class GlobalView extends React.Component {

  constructor(props, context) {
    super(props);
    this.lessonId = props.match.params.lessonId;
    this.vocabulary = vocabulary;
  }

  static childContextTypes = {
    currentLessonId: PropTypes.string.isRequired,
    currentLessonData: PropTypes.array.isRequired,
    vocabulary: PropTypes.object
  };

  getChildContext() {
    return {
      currentLessonId: this.lessonId,
      currentLessonData: get(LessonsData, this.lessonId),
      vocabulary: get(this.vocabulary, 'pronouns')
    };
  }

  render() {
    return (
      <div className="App">
        <HeaderComponent/>
        <div className="container">
          <NativeText/>
          <TranslationTextArea
            className="translation-input"
            translation={'state'}
          />
          <AnswersVariants/>
        </div>
      </div>
    );
  }
}

export default GlobalView;