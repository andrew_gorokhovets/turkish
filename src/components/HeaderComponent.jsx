import React from 'react';
import PropTypes from 'prop-types';

/**
 * @todo удалить vocabulary: PropTypes.object после дебагинга.
 */
const HeaderComponent = (props, context) => {
  return (
    <div className="App-header">
      <h2>Урок {context.currentLessonId}</h2>
    </div>
  );
};

HeaderComponent.contextTypes = {
  currentLessonId: PropTypes.string,
  currentLessonData: PropTypes.array,
  vocabulary: PropTypes.object
};

export default HeaderComponent;