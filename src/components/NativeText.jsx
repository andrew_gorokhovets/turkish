import React from 'react';

import SentenceStructure from 'components/SentenceStructure';

const NativeText = () => (
  <div className="native-text">
    <SentenceStructure/>
  </div>
);

export default NativeText;