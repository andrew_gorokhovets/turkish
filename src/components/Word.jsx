import React from 'react';

const Word = (props) => (
  <span>{props.value}</span>
);

export default Word;