import React from 'react';
import {Button} from 'react-bootstrap';

import getRandomTerms from 'lib/getRandomTerms';
import Qualities from 'data/Adjectives/Qualities';

const AnswersVariants = () => (
  <div className="answers-variant">
    {
      getRandomTerms(Qualities, 8).map(function (el) {
        return <Button
          bsStyle="link"
          value={el.native} key={el.id}
          onClick={handleClick}>{el.native}
        </Button>;
      })
    }
  </div>
);

function handleClick(event) {
  console.log(event);
}

export default AnswersVariants;