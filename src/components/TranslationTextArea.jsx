import React from 'react';
import {Button, FormControl, FormGroup} from 'react-bootstrap';

const TranslationTextArea = (props) => (
  <form>
    <FormGroup controlId="formControlsTextarea">
      <FormControl
        componentClass="textarea"
        placeholder="Переведите текст"
      />
    </FormGroup>
    <Button type="submit">Проверить</Button>
  </form>
);

export default TranslationTextArea;