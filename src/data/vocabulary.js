/**
 * Contains Pronouns Object
 * @type {Object}
 */

export default {
  pronouns: {
    prn1: {
      id: 'prn1',
      foreign: 'ben',
      native: 'я'
    },
    prn2: {
      id: 'prn2',
      foreign: 'sen',
      native: 'ты'
    },
    prn3: {
      id: 'prn3',
      foreign: 'o',
      native: ['он', 'она']
    },
    prn4: {
      id: 'prn4',
      foreign: 'biz',
      native: 'мы'
    },
    prn5: {
      id: 'prn5',
      foreign: 'siz',
      native: 'вы'
    },
    prn6: {
      id: 'prn6',
      foreign: 'onlar',
      native: 'они'
    }
  },
  adjectives: {
    qualities: {
      qlt1: {
        id: 'qlt1',
        foreign: 'kötü',
        native: 'плохой'
      },
      qlt2: {
        id: 'qlt2',
        foreign: 'temiz',
        native: 'чистый'
      },
      qlt3: {
        id: 'qlt3',
        foreign: 'karanlık',
        native: ['темный']
      },
      qlt4: {
        id: 'qlt4',
        foreign: 'zor',
        native: 'трудный'
      },
      qlt5: {
        id: 'qlt5',
        foreign: 'kirli',
        native: 'грязный'
      },
      qlt6: {
        id: 'qlt6',
        foreign: 'kuru',
        native: 'сухой'
      },
      qlt7: {
        id: 'qlt7',
        foreign: 'kolay',
        native: 'легко'
      },
      qlt8: {
        id: 'qlt8',
        foreign: 'boş',
        native: 'пустой'
      },
      qlt9: {
        id: 'qlt9',
        foreign: 'pahalı',
        native: 'дорогой'
      },
      qlt10: {
        id: 'qlt10',
        foreign: 'hızlı',
        native: 'быстрый'
      },
      qlt11: {
        id: 'qlt11',
        foreign: 'yabancı',
        native: 'иностранный'
      },
      qlt12: {
        id: 'qlt12',
        foreign: ['tam', 'noksansız', 'dolu'],
        native: 'полный'
      },
      qlt13: {
        id: 'qlt13',
        foreign: 'iyi',
        native: 'хороший'
      },
      qlt14: {
        id: 'qlt14',
        foreign: 'sert',
        native: 'жесткий'
      },
      qlt15: {
        id: 'qlt15',
        foreign: 'ağır',
        native: 'тяжелый'
      },
      qlt16: {
        id: 'qlt16',
        foreign: 'ucuz',
        native: 'недорогой'
      },
      qlt17: {
        id: 'qlt17',
        foreign: 'hafif',
        native: 'свет'
      },
      qlt18: {
        id: 'qlt18',
        foreign: 'yerel',
        native: 'местный'
      },
      qlt19: {
        id: 'qlt19',
        foreign: 'yeni',
        native: 'новый'
      }
    }
  }
};