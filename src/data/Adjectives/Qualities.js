/**
 * Contains Qualities Object.
 *
 * See http://mylanguages.org/ru/turkish_adjectives.php
 *
 * @type {Object}
 */

var Qualities = [
  {
    id: 'Qualities-1',
    foreign: 'kötü',
    native: 'плохой'
  },
  {
    id: 'Qualities-2',
    foreign: 'temiz',
    native: 'чистый'
  },
  {
    id: 'Qualities-3',
    foreign: 'karanlık',
    native: ['темный']
  },
  {
    id: 'Qualities-4',
    foreign: 'zor',
    native: 'трудный'
  },
  {
    id: 'Qualities-5',
    foreign: 'kirli',
    native: 'грязный'
  },
  {
    id: 'Qualities-6',
    foreign: 'kuru',
    native: 'сухой'
  },
  {
    id: 'Qualities-7',
    foreign: 'kolay',
    native: 'легко'
  },
  {
    id: 'Qualities-8',
    foreign: 'boş',
    native: 'пустой'
  },
  {
    id: 'Qualities-9',
    foreign: 'pahalı',
    native: 'дорогой'
  },
  {
    id: 'Qualities-10',
    foreign: 'hızlı',
    native: 'быстрый'
  },
  {
    id: 'Qualities-11',
    foreign: 'yabancı',
    native: 'иностранный'
  },
  {
    id: 'Qualities-12',
    foreign: ['tam', 'noksansız', 'dolu'],
    native: 'полный'
  },
  {
    id: 'Qualities-13',
    foreign: 'iyi',
    native: 'хороший'
  },
  {
    id: 'Qualities-14',
    foreign: 'sert',
    native: 'жесткий'
  },
  {
    id: 'Qualities-15',
    foreign: 'ağır',
    native: 'тяжелый'
  },
  {
    id: 'Qualities-16',
    foreign: 'ucuz',
    native: 'недорогой'
  },
  {
    id: 'Qualities-17',
    foreign: 'hafif',
    native: 'свет'
  },
  {
    id: 'Qualities-18',
    foreign: 'yerel',
    native: 'местный'
  },
  {
    id: 'Qualities-19',
    foreign: 'yeni',
    native: 'новый'
  }
];

export default Qualities;