import {default as _} from 'data/vocabulary';

/**
 * @todo Написать функцию которая будет возвтращать данные по уроку
 * на основе LessonsData,
 * Использовать {getRandomTerm(vocabulary.vocabulary).native}
 */
export default {
  1: [_.pronouns, _.adjectives.qualities],
  2: [_.adjectives.qualities, _.pronouns]
};